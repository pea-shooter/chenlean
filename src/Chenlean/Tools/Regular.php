<?php

namespace Chenlean\Tools;


class Regular
{

    protected function regex($REGMatch, $str)
    {
        return preg_match($REGMatch, $str);
    }

    /**IPv4地址
     * @param $str
     * @return false|int
     */
    public function RegularIPv4($str)
    {
        return self::regex('/((2(5[0-5]|[0-4]\d))|[0-1]?\d{1,2})(\.((2(5[0-5]|[0-4]\d))|[0-1]?\d{1,2})){3}/', $str);
    }

    /** 邮编
     * @param $str
     * @return false|int
     */
    public function RegularPostCode($str)
    {
        return self::regex('/[1-9]\d{5}(?!\d)/', $str);
    }

    /**腾讯QQ号
     * @param $str
     * @return false|int
     */
    public function RegularQQ($str)
    {
        return self::regex('/[1-9][0-9]{4,}/', $str);
    }

    /**xml文件
     * @param $str
     * @return false|int
     */
    public function RegularXml($str)
    {
        return self::regex('/^([a-zA-Z]+-?)+[a-zA-Z0-9]+\\.[x|X][m|M][l|L]$/', $str);
    }

    /** 日期格式
     * @param $str
     * @return false|int
     */
    public function RegularDate($str)
    {
        return self::regex('/^\d{4}-\d{1,2}-\d{1,2}/', $str);
    }

    /**强密码(必须包含大小写字母和数字的组合，可以使用特殊字符，长度在8-10之间)
     * @param $str
     * @return false|int
     */
    public function RegularStrongpwd($str)
    {
        return self::regex('/^(?=.*\d)(?=.*[a-z])(?=.*[A-Z]).{8,10}$/', $str);
    }

    /**密码
     * @param $str
     * @return false|int
     */
    public function Regularpwd($str)
    {
        return self::regex('/^[a-zA-Z]\w{5,17}$/', $str);
    }

    /**帐号是否合法(字母开头，允许5-16字节，允许字母数字下划线)
     * @param $str
     * @return false|int
     */
    public function RegularAccount($str)
    {
        return self::regex('/^[a-zA-Z][a-zA-Z0-9_]{4,15}$/', $str);
    }

    /** 身份证号码
     * @param $id
     * @return false|int
     */
    public function RegularIDCard($id)
    {
        return self::regex('/(^\d{15}$)|(^\d{18}$)|(^\d{17}(\d|X|x)$)/', $id);
    }

    /**电话号码
     * @param $str
     * @return false|int
     */
    public function RegularLinePhone($str)
    {
        return self::regex('/^(\(\d{3,4}-)|\d{3.4}-)?\d{7,8}$/', $str);
    }

    /** 手机号码
     * @param $tel
     * @return false|int
     */
    public function RegularTel($tel)
    {
        return self::regex('/^(13[0-9]|14[5|7]|15[0|1|2|3|4|5|6|7|8|9]|18[0|1|2|3|5|6|7|8|9])\d{8}$/', $tel);
    }

    /**域名
     * @param $str
     * @return false|int
     */
    public function RegularDomain($str)
    {
        return self::regex('/[a-zA-Z0-9][-a-zA-Z0-9]{0,62}(\.[a-zA-Z0-9][-a-zA-Z0-9]{0,62})+\.?/', $str);
    }

    /**Email地址
     * @param $email
     * @return false|int
     */
    public function RegularEmail($email)
    {
        return self::regex('/^\w+([-+.]\w+)*@\w+([-.]\w+)*\.\w+([-.]\w+)*$/', $email);
    }

    /**数字、26个英文字母或者下划线组成的字符串
     * @param $str
     * @return false|int
     */
    public function RegularCharMix($str)
    {
        return self::regex('/^\w{3,20}$/', $str);
    }

    /** 26个小写字母
     * @param $str
     * @return false|int
     */
    public function RegularLowerEnglish($str)
    {
        return self::regex('/^[a-z]+$/', $str);
    }

    /** 26个大写字母
     * @param $str
     * @return false|int
     */
    public function RegularCapitalEnglish($str)
    {
        return self::regex('/^[A-Z]+$/', $str);
    }

    /** 英文和数字
     * @param $str
     * @return false|int
     */
    public function RegularEnglishNumber($str)
    {
        return self::regex('/^[A-Za-z0-9]+$/', $str);
    }

    /** 汉字
     * @param $str
     * @return false|int
     */
    public function RegularChinese($str)
    {
        return self::regex('/^[\u4e00-\u9fa5]{0,}$/', $str);
    }

    /** 浮点数
     * @param $number
     * @return false|int
     */
    public function RegularNumber13($number)
    {
        return self::regex('/^(-?\d+)(\.\d+)?$/', $number);
    }

    /**负浮点数
     * @param $number
     * @return false|int
     */
    public function RegularNumber12($number)
    {
        return self::regex('/^-([1-9]\d*\.\d*|0\.\d*[1-9]\d*)$ /', $number);
    }

    /**正浮点数
     * @param $number
     * @return false|int
     */
    public function RegularNumber11($number)
    {
        return self::regex('/^[1-9]\d*\.\d*|0\.\d*[1-9]\d*$ /', $number);
    }

    /**非正浮点数
     * @param $number
     * @return false|int
     */
    public function RegularNumber10($number)
    {
        return self::regex('/^((-\d+(\.\d+)?)|(0+(\.0+)?))$/', $number);
    }

    /** 非负浮点数
     * @param $number
     * @return false|int
     */
    public function RegulatNumber9($number)
    {
        return self::regex('/^\d+(\.\d+)?$/', $number);
    }

    /** 非正整数
     * @param $number
     * @return false|int
     */
    public function RegularNumber8($number)
    {
        return self::regex('/^-[1-9]\d*|0$/', $number);
    }

    /*** 带1-2位小数的正数或负数
     * @param $number
     * @return false|int
     */
    public function RegularNumber7($number)
    {
        return self::regex('/^(\-)?\d+(\.\d{1,2})$/', $number);
    }

    /** 非负整数
     * @param $number
     * @return false|int
     */
    public function RegularNumber6($number)
    {
        return self::regex('/^\d+$/', $number);
    }

    /***  非零的负整数
     * @param $number
     * @return false|int
     */
    public function RegularNumber5($number)
    {
        return self::regex('/^\-[1-9][]0-9"*$/', $number);
    }

    /*** 非零的正整数
     * @param $number
     * @return false|int
     */
    public function RegularNumber4($number)
    {
        return self::regex('/^[1-9]\d*$/', $number);
    }

    /*** 有1~3位小数的正实数
     * @param $number
     * @return false|int
     */
    public function RegularNumber3($number)
    {
        return self::regex('/^[0-9]+(\.[0-9]{1,3})?$/', $number);
    }

    /*** 有两位小数的正实数
     * @param $number
     * @return false|int
     */
    public function RegularNumber2($number)
    {
        return self::regex('/^[0-9]+(\.[0-9]{2})?$/', $number);
    }

    /*** 正数、负数、和小数
     * @param $number
     * @return false|int
     */
    public function RegularNumber1($number)
    {
        return self::regex('/^(\-|\+)?\d+(\.\d+)?$/', $number);
    }

    /*** 非零开头的最多带两位小数的数字
     * @param $number
     * @return false|int
     */
    public function RegularTwoPointNumber($number)
    {
        return self::regex('/^([1-9][0-9]*)+(\.[0-9]{1,2})?$/', $number);
    }

    /*** 零和非零开头的数字
     * @param $number
     * @return false|int
     */
    public function RegularZeroPreNumber($number)
    {
        return self::regex('/^(0|[1-9][0-9]*)$/', $number);
    }

    /**  m至n位数字
     * @param $number
     * @param $m
     * @param $n
     * @return false|int
     */
    public function RegularNMNumber($number, $m, $n)
    {
        return self::regex('/^\d{' . $m . ',' . $n . '}$/', $number);
    }

    /**n位数字
     * @param $number
     * @param $n
     * @return false|int
     */
    public function RegularNNumber($number, $n)
    {
        return self::regex('/^\d{' . $n . '}$/', $number);
    }

    /*** 数字
     * @param $number
     * @return false|int
     */
    public function RegularNumber($number)
    {
        return self::regex('/^[0-9]*$/', $number);
    }

}
